# -*- coding: utf-8 -*-


import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()

score_hist_3 = 0

if form.getvalue("question_hist_3"):
    print("Content-type: text/html; charset=UTF_8\n")
    
    html = """<!doctype html>
    <html lang=fr>
        <head>
            <title> Partie Histoire : Question 3</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
        
           <h5> Troisième question :</h5>
           <form class="quiz-form" method="post" action="question_hist_4.py">
                <h2> Quand a été fondée la ville de Sydney, en Australie ? </h2>
                <p><input type="radio" name="date_creation" value="rep_3_1" class="question"/> <label for="date_creation">49 Février 2038</label><br/></p>
                <p><input type="radio" name="date_creation" value="rep_3_2" class="question"/> <label for="date_creation">16 Juillet 1915</label><br/></p>
                <p><input type="radio" name="date_creation" value="rep_3_3" class="question"/> <label for="date_creation">26 Janvier 1788</label><br/></p>
                <p><input type="radio" name="date_creation" value="rep_3_4" class="question"/> <label for="date_creation">3 Octobre 1003</label><br/></p>
                <p><input type="radio" name="date_creation" value="rep_3_5" class="question"/> <label for="date_creation">Vers 600 avant J.-C.</label><br/></p>
                <br/><br/>
                <input type="submit" value="Question suivante" name="question_hist_4" class="qsuivante"/>
            </form>
        </body>
    </html>"""

    
    print(html)

"""
if form.getvalue("rep_3_3"):
    score_hist_3 = score_hist_3 + 5
    
else:
    score_hist_3 -= 5
"""


open('resultats.csv', 'r')

if form.getvalue("date_creation"):
    dates = form.getvalue("date_creation")
    if dates == '26 Janvier 1788':
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass




















