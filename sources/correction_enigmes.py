# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_enigmes"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Enigmes</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h1> Puis la correction des énigmes ! </h1>
           <form method="post" action="correction_enigme_1.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_enigme_1"/>
           </form> 
        </body>
    </html>
    """

    print(html)
