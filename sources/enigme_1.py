# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()

score_enigme_1 = 0

if form.getvalue("enigme_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie Enigmes : Question 1</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h4> Si on m'appelle par mon nom, je disparaîs. Qui suis-je ?</h4>
           <form method="post" action="enigme_2.py">
               <p><label for="rep_E_1"> Votre proposition : </label> <input type="text" name="rep_E_1" required/>  </p>
 </br>              <input type="submit" value="Question suivante" class="qsuivante" name="enigme_2"/>
           </form> 
        </body>
    </html>
    """

    print(html)

if form.getvalue("rep_E_1") == 'Le silence':
    score_enigme_1 += 5
else:
    score_enigme_1 -= 5
    

open('resultats.csv', 'r')

if form.getvalue("rep_E_1"):
    reponse = form.getvalue("rep_E_1")
    if reponse == "Le silence":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass












