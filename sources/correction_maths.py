# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_maths"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Maths</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h5> Voici la correction des questions de maths ! </h5>
           <form method="post" action="correction_maths_1.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_maths_1"/>
           </form> 
        </body>
    </html>
    """

    print(html)

