# -*- coding: utf-8 -*-

import cgitb 
import cgi
import csv

cgitb.enable()
form = cgi.FieldStorage()

score = None  # Initialisation de la variable score

with open('resultats.csv', 'r') as file:
    reader = csv.reader(file)
    
    # Passer à la première ligne du fichier CSV
    try:
        first_row = next(reader)
        score = first_row[0]  # Extraire la donnée de la première colonne de la première ligne
    except StopIteration:
        pass

print("Content-Type: text/html\n")  # En-tête pour indiquer que le contenu est HTML

html_1 = """<!DOCTYPE html>
<html>
    <head>
        <title> Vos résultats </title>
        <meta charset="UTF_8"/>
        <link rel="stylesheet" href="css_js/style1.css">
    </head>
    
    <body>
        <h1> Voici vos résultats </h1>
        
"""
print(html_1)

if score is not None:
    print(f"""<h2> Votre score final est de {score} ! </h2>""")
else:
    print("<h2> Aucun score trouvé dans le fichier CSV. </h2>")

html_2 = f"""<h2> Votre QI est de {score} </h2>
        
        <form method="post" action="correction_histoire.py">               
            <input type="submit" class="qsuivante" value="La suite !" name="correction_histoire"/>
        </form> 
    </body>
</html>"""

print(html_2)