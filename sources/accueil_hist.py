# -*- coding: utf-8 -*-

import cgitb
import cgi


# débogage
cgitb.enable()

# récupération données formulaire
form = cgi.FieldStorage()

if form.getvalue("accueil_hist"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="en">
        <head>
          <meta charset="UTF_8">
          <title>Accueil Histoire</title>
          <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
            <h5>Bienvenue sur la page qui précède les questions d'histoire.</h5>
            <h2>Lorsque vous cliquerez sur "Continuer", vous serez en face de 5 QCM</h2>
            <h2> Attention : toutes recherches sur internet invalidera de votre résultat final au test !</h2>
            <form method="post" action="question_hist_1.py">
                <input type="submit" value="Continuer" class="continuerhist" name="question_hist_1">
            </form>
          </body>
    </html>
    """
    
    print(html)