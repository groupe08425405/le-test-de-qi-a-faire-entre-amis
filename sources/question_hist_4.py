# -*- coding: utf-8 -*-


import cgi
import cgitb
import csv



cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("question_hist_4"):
    print("Content-type: text/html; charset=UTF_8\n")
    
    html = """<!doctype html>
    <html lang=fr>
        <head>
            <title> Partie Histoire : Question 4</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
        
           <h5> Quatrième question :</h5>
           <form class="quiz-form" method="post" action="question_hist_5.py">
                <h2> Qui était François Arago ? </h2>
                <p><input type="radio" name="fonctionFA" value="rep_4_1" class="question"/> <label for="fonctionFA">Un chimiste Suisse</label><br/></p>
                <p><input type="radio" name="fonctionFA" value="rep_4_2" class="question"/> <label for="fonctionFA">Un écrivain français</label><br/></p>
                <p><input type="radio" name="fonctionFA" value="rep_4_3" class="question"/> <label for="fonctionFA">Un critique littéraire Belge</label><br/></p>
                <p><input type="radio" name="fonctionFA" value="rep_4_4" class="question"/> <label for="fonctionFA">Un mathématicien Suisse</label><br/></p>
                <p><input type="radio" name="fonctionFA" value="rep_4_5" class="question"/> <label for="fonctionFA">Un homme d'État français</label><br/></p>
                <br/><br/>
                <input type="submit" value="Question suivante" name="question_hist_5" class="qsuivante"/>
            </form>
        </body>
    </html>"""

    
    print(html)

"""
if form.getvalue("rep_4_5"):
    score_hist_4 = score_hist_4 + 5
    
else:
    score_hist_4 -= 5
"""

open('resultats.csv', 'r')

if form.getvalue("fonctionFA"):
    dates = form.getvalue("fonctionFA")
    if dates == "Un homme d'État français":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass








