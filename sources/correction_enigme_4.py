# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_enigme_4"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Enigme 4</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Un gant. C'était la réponse attendue. En effet le gant possède 5 doigts et n'est pas une partie du corps.</h2>
           <form method="post" action="correction_enigme_5.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_enigme_5"/>
           </form> 
        </body>
    </html>
    """

    print(html)