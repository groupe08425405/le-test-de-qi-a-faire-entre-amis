# -*- coding: utf-8 -*-



import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_JO_2"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction JO : Question 2</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <p> Correction question 2 sur JO </p>
           <form method="post" action="correction_JO_3.py">               
               <input type="submit" class="qsuivante" value="Voir mes résultats" name="correction_JO_3"/>
           </form> 
        </body>
    </html>
    """

    print(html)