# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_hist_4"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Histoire : Question 4 </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> François Arago, est un astronome, physicien et homme d'Etat français né en 1786 et mort en 1853. </h2>
           <h2> Pendant les Trois Glorieuses, il est colonel de la Garde nationale. Après la révolution de 1848, il devient ministre de la Guerre, dela marine et des colonies, dans le gouvernement provisoire de la Seconde République. </h2>
           <p><img src="Images/Francois_Arago.jpg"/></p>
           <form method="post" action="correction_hist_5.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_hist_5"/>
           </form> 
        </body>
    </html>
    """

    print(html)