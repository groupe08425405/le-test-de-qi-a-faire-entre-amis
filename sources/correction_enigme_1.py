# -*- coding: utf-8 -*-



import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_enigme_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Enigme 1</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Pour l'énigme 1, il s'agit du silence. En effet, lorsqu'on prononce son nom, on rompt le silence ! </h2>
           <form method="post" action="correction_enigme_2.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_enigme_2"/>
           </form> 
        </body>
    </html>
    """

    print(html)
