# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("accueil_enigmes"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie Maths : Question 5</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           
        <h4> Vous avez terminé la partie mathématiques. Désormais, vous devez répondre à des énigmes !</h4>
        
           <form method="post" action="enigme_1.py">
               <input type="submit" value="Question suivante" class="qsuivante" name="enigme_1"/>
           </form> 
        </body>
    </html>
    """

print(html)