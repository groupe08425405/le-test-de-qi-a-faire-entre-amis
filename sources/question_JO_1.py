# -*- coding: utf-8 -*-


import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()

score_JO_1 = 0

if form.getvalue("question_JO_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie JO : Question 1 </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h1> Combien de sport seront représentés lors des Jeux Olympiques (JO) de Paris 2024 ?</h1>
           <form method="post" action="question_JO_2.py">
               <p><label for="rep_JO_1"> Votre proposition : </label> <input type="text" name="rep_JO_1" required/></p></br>
               <input type="submit" value="Question suivante"class="qsuivante" name="question_JO_2"/>
           </form> 
        </body>
    </html>
    """

    print(html)

if form.getvalue("rep_JO_1") == '32':
    score_JO_1 += 5
else:
    score_JO_1 -= 5
 


if form.getvalue("rep_JO_1"):
    reponse = form.getvalue("rep_JO_1")
    if reponse == "32":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass


