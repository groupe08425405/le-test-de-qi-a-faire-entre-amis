# -*- coding: utf-8 -*-





import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_enigme_2"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Enigme 2</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> En ce qui concerne celle-ci, c'était le stylo ! Le stylo peut s'exprimer dans n'importe quelle langue (et peut même les créer). </h2>
           <form method="post" action="correction_enigme_3.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_enigme_3"/>
           </form> 
        </body>
    </html>
    """

    print(html)