# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_maths_2"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Maths : Question 2</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h5> Dans cette question, il fallait : </h5>
           <h2>Soit reconnaître, du premier coup d'oeil, la suite de Fibonacci, dont le terme suivant se détermine grâce à la somme des deux termes précédents. </h2>
           <h2> Soit, trouver la logique qui se cache derrière cette suite de nombre. </h2>
           <h2> La réponse correct est donc : 13 + 21 (la somme des deux termes précédents) = 34</h2>
           <form method="post" action="correction_maths_3.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_maths_3"/>
           </form> 
        </body>
    </html>
    """

    print(html)
