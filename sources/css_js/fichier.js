// Mord Pion

let cells = ['', '', '', '', '', '', '', '', ''];
let joueurActuel = 'X';
let result = document.querySelector('.result');
let btns = document.querySelectorAll('.btn');
let conditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];
const mordPion = (element, index) => {
    element.value = joueurActuel;
    element.disabled = true;
    cells[index] = joueurActuel;
    joueurActuel = joueurActuel == 'X' ? 'O' : 'X';
    result.innerHTML = `Tour de :${joueurActuel}`;
    for (let i = 0; i < conditions.length; i++) {
        let condition = conditions[i];
        let a = cells[condition[0]];
        let b = cells[condition[1]];
        let c = cells[condition[2]];
        if (a == '' || b == '' || c == '') {
            continue;
        }
        if ((a == b) && (b == c)) {
            result.innerHTML = `Le joueur ${a} a gagn� !`;
            btns.forEach((btn) => btn.disabled = true);
        }
    }
};
function reset() {
    cells = ['', '', '', '', '', '', '', '', ''];
    btns.forEach((btn) => {
        btn.value = '';
    });
    joueurActuel = 'X';
    result.innerHTML = `Tour de : X`;
    btns.forEach((btn) => btn.disabled = false);
};
document.querySelector('#reset').addEventListener('click', reset);
btns.forEach((btn, i) => {
    btn.addEventListener('click', () => mordPion(btn, i));
});