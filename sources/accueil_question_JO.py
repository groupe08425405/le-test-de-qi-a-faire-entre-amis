# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()

score_enigme_5 = 0

if form.getvalue("accueil_question_JO"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Accueil JO </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h1> Enfin les dernières questions ! Vous y êtes presque ! Désormais, voici les questions sur les Jeux Olympiques (JO) ! </h1>
           <form method="post" action="question_JO_1.py">
               <input type="submit" class="qsuivante" value="Question suivante" name="question_JO_1"/>
           </form> 
        </body>
    </html>
    """

    print(html)