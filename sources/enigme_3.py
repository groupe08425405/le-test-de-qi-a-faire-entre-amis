# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()

score_enigme_3 = 0

if form.getvalue("enigme_3"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie Enigmes : Question 3 </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h4> Une famille compte deux parents et six fils. Chacun des fils a une soeur. Combien y a-t-il de personnes dans cette familles ?</h4>
           <form method="post" action="enigme_4.py">
               <p><label for="rep_E_3"> Votre proposition : </label> <input type="text" name="rep_E_3" required/>  </p></br>
               <input type="submit" value="Question suivante" class="qsuivante" name="enigme_4"/>
           </form> 
        </body>
    </html>
    """

    print(html)
    
    
"""
if form.getvalue("rep_E_3") == '9':
    score_enigme_3 += 5
else:
    score_enigme_3 -= 5
"""


if form.getvalue("rep_E_3"):
    reponse = form.getvalue("rep_E_3")
    if reponse == "9":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass