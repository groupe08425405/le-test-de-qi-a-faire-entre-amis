# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()



if form.getvalue("question_maths_5"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie Maths : Question 5</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h6>Quelle est la forme développée de l'expression littérale suivante ?</h6>
           <p><img src="Images/maths_5.jpg"/></p>
           <h2> Remarque : dans votre réponse, utilisez bien les caractères "x" et "y" et utilisez "^" pour exprimer les nombres en exposant.</br> De plus, vous devrez faire des espaces entre les signes "+"</h2>
           <p>Par exemple, pour (a+b)², vous auriez dû marquer : a^2 + 2ab + b^2 </p>
           <form method="post" action="accueil_enigmes.py">
               <p><label for="rep_dev"> Votre proposition : </label> <input type="text" name="rep_dev" required/></p></br>
               <input type="submit" value="Question suivante" class="qsuivante" name="accueil_enigmes"/>
           </form> 
        </body>
    </html>
    """

    print(html)
"""
if form.getvalue("rep_dev") == 'x^4 + 4x^3y + 6x^2y^2 + 4xy^3 + y^4':
    score_maths_5 += 5
else:
    score_maths_5 -= 0
"""

open('resultats.csv', 'r')

if form.getvalue("rep_dev"):
    reponse = form.getvalue("rep_dev")
    if reponse == "x^4 + 4x^3y + 6x^2y^2 + 4xy^3 + y^4":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    