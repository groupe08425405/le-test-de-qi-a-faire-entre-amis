# -*- coding: utf-8 -*-


import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()

score_JO_3 = 0

if form.getvalue("question_JO_3"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie JO : Question 3 </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h1> Quand se déroulent les JO de Paris 2024? </h1>
           <form method="post" action="pre_resultats.py">
               <p><input type="radio" name="moment_JO" value="rep__JO_3_1"/> <label for="moment_JO">De Novembre à Janvier</label><br/>
               <input type="radio" name="moment_JO" value="rep_JO_3_2"/> <label for="moment_JO">De Juillet à Août</label><br/>
               <input type="radio" name="moment_JO" value="rep_JO_3_3"/> <label for="moment_JO">De Avril à Juin</label><br/>
               <input type="radio" name="moment_JO" value="rep_JO_3_4"/> <label for="moment_JO">De Janvier à Décembre</label><br/></p>    
               
               <input type="submit" class="qsuivante" value="Question suivante" name="pre_resultats"/>
           </form> 
        </body>
    </html>
    """

    print(html)


    

if form.getvalue("rep_JO_3"):
    reponse = form.getvalue("rep_JO_3")
    if reponse == "De Juillet à Août":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass