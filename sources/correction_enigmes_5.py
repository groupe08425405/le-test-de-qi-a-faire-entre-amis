# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_enigme_5"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Enigme 5</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <p> Cette énigme était sans aucun doute la plus difficile. Il fallait certainement s'aider d'un dessin ou d'un schéma pour y parvenir. </p>
           <p> Il s'agit de mon père.</p>
           <form method="post" action="correction_JO.py">               
               <input type="submit" value="Voir mes résultats" name="correction_JO"/>
           </form> 
        </body>
    </html>
    """

    print(html)