# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()



if form.getvalue("question_hist_5"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!doctype html>
    <html lang=fr>
        <head>
            <title> Partie Histoire : Question 5</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
        
           <h5> Cinquième et dernière question :</h5>
           <form class="quiz-form" method="post" action="accueil_maths.py">
                <h2> Qui a fondé les jeux olympiques modernes et en quelle année ? </h2>
                <p><input type="radio" name="JO_Mod" value="rep_5_1" class="question"/> <label for="JO_Mod">Pierre de Coubertin en 1894</label><br/></p>
                <p><input type="radio" name="JO_Mod" value="rep_5_2" class="question"/> <label for="JO_Mod">Louis Braille en 1849</label><br/></p>
                <p><input type="radio" name="JO_Mod" value="rep_5_3" class="question"/> <label for="JO_Mod">Claude-François Denecourt en 1874</label><br/></p>
                <p><input type="radio" name="JO_Mod" value="rep_5_4" class="question"/> <label for="JO_Mod">David Gruber en 1880</label><br/></p>
                <p><input type="radio" name="JO_Mod" value="rep_5_5" class="question"/> <label for="JO_Mod">Marcelin Berthelot en 1902</label><br/></p>
                <p><br/><br/></p>
                <input type="submit" value="Question suivante" name="accueil_maths" class="qsuivante"/>
            </form>
        </body>
    </html>"""
    
    print(html)
"""
if form.getvalue("rep_5_1"):
    score_hist_5 = score_hist_5 + 5
    
else:
    score_hist_5 -= 5
"""

open('resultats.csv', 'r')

if form.getvalue("JO_Mod"):
    dates = form.getvalue("JO_Mod")
    if dates == "Pierre de Coubertin en 1894":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass












