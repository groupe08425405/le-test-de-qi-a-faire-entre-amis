# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_hist_2"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Histoire : Question 2 </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Louis IX, plus communément appelé Saint Louis, est un roi de France capétien né en 1214 et mort en 1270.</h2>
           <h2> Il régna pendant plus de 43 ans, de 1226 jusqu'à sa mort.</h2>
           <p><img src="Images/Saint_Louis.jpg"/></p>
           <form method="post" action="correction_hist_3.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_hist_3"/>
           </form> 
        </body>
    </html>
    """

    print(html)