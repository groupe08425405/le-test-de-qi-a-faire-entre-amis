# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("accueil_maths"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!doctype html>
    <html lang=fr>
        <head>
            <title>Accueil Maths</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
            <h6>Vous avez terminé la partie Histoire. Maintenant vous allez faire face aux questions de maths !</h6>
           
           <form method="post" action="question_maths_1.py">
                <input type="submit" class="qsuivante" value="Question suivante" name="question_maths_1"/>
            </form>
        </body>
    </html>"""
    
print(html)
