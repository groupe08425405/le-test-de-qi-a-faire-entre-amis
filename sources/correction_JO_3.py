# -*- coding: utf-8 -*-



import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_JO_3"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction JO : Question 3</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style.css">
        </head>
        <body>
           <p>Correction derniere question JO = 3  </p>
           <form method="post" action="remerciements.py">               
               <input type="submit" value="Voir mes résultats" name="remerciements"/>
           </form> 
        </body>
    </html>
    """

    print(html)