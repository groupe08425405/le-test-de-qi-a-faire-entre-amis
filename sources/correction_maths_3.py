# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_maths_3"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Maths : Question 3</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> CPour déterminer les solutions de l'équation de cette question, nous pouvions procéder de 2 manière différentes. </h2>
           <h2> La première (aussi la plus simple), on pouvait procéder par élimination. C'est-à-dire, remplacer l'inconnue de l'équation par les valeur proposées, puis effectuer le calcul pour savoir si le résultat obtenu est bien 0. Dans ce cas la valeur est considérée comme une solution.</h2>
           <h2>La seconde (la plus calculatoire), est d'utiliser la méthode du "discriminant".</h2>
           <p><img src="Images/correction_maths_3.jpg"/></p>
           <p><img src="Images/correction_maths_3_bis.jpg"/></p>
           <form method="post" action="correction_maths_4.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_maths_4"/>
           </form> 
        </body>
    </html>
    """

    print(html)
