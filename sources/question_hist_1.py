# -*- coding: utf-8 -*-

# questionnaire histoire : question 1

import csv
import cgi
import cgitb


# débogage
cgitb.enable()

# récupération données formulaire
form = cgi.FieldStorage()


if form.getvalue("question_hist_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!doctype html>
    <html lang=fr>
    <head>
        <title>Partie Histoire : Question 1</title>
        <meta charset="UTF_8"/>
        <link rel="stylesheet" href="css_js/style1.css">
    </head>
    <body>
    
       <h5 class="page-title"> Première question :</h5>
        <form class="quiz-form" method="post" action="question_hist_2.py">
            <h2> Quelle est la date de naissance de Napoléon Bonaparte ?</h2>
            <p><input type="radio" name="date_naissance" value="rep_1_1" class="question"/> <label for="date_naissance"> 2 décembre 1805 </label><br/></p>
            <p><input type="radio" name="date_naissance" value="rep_1_2" class="question"/> <label for="date_naissance">15 août 1769</label><br/></p>
            <p><input type="radio" name="date_naissance" value="rep_1_3" class="question"/> <label for="date_naissance"> 18 juin 1815 </label><br/></p>
            <p><input type="radio" name="date_naissance" value="rep_1_4" class="question"/> <label for="date_naissance"> 29 février 1769 </label><br/></p>
            <p><input type="radio" name="date_naissance" value="rep_1_5" class="question"/> <label for="date_naissance"> 6 juin 1944 </label><br/></p>
            <br/><br/>
            
            <input type="submit" value="Question suivante" name="question_hist_2" class="qsuivante"/>
        </form>
    
    </body>
    </html>"""

    print(html)

"""
if form.getvalue("rep_1_2"):
    score_hist_1 = score_hist_1 + 5
    
else:
    score_hist_1 -= 5
"""


open('resultats.csv', 'r')

    
    
if form.getvalue("date_naissance"):
    date_1 = form.getvalue("date_naissance")
    if date_1 == '15 août 1769':
        score = 5
        data = score
        fichier_csv = open('resultats.csv', 'w')
        obj = csv.writer(fichier_csv)
        obj.write(data)
    else:
        score = -5
        data = score
        fichier_csv = open('resultats.csv', 'w')
        obj = csv.writer(fichier_csv)
        obj.write(data)
    
    
else:
    pass
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    