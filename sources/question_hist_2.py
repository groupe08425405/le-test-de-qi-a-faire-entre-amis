# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("question_hist_2"):
    print("Content-type: text/html; charset=UTF_8\n")
    
    html = """<!doctype html>
    <html lang=fr>
    <head>
        <title>Partie Histoire : Question 2</title>
        <meta charset="UTF_8"/>
        <link rel="stylesheet" href="css_js/style1.css">
    </head>
    <body>
    
   <h5> Deuxième question :</h5>
    <form class="quiz-form" method="post" action="question_hist_3.py">
        <h2> Quelles sont les dates du règne de Saint Louis ?</h2>
        <p><input type="radio" name="date_regne" value="rep_2_1" class="question"/> <label for="date_regne">1760 à 1765</label><br/></p>
        <p><input type="radio" name="date_regne" value="rep_2_2" class="question"/> <label for="date_regne">2017 à 2022</label><br/></p>
        <p><input type="radio" name="date_regne" value="rep_2_3" class="question"/> <label for="date_regne">512 à 600</label><br/></p>
        <p><input type="radio" name="date_regne" value="rep_2_4" class="question"/> <label for="date_regne">1226 à 1270</label><br/></p>
        <p><input type="radio" name="date_regne" value="rep_2_5" class="question"/> <label for="date_regne">1356 à 1406</label><br/></p>
        <br/><br/>
        <input type="submit" value="Question suivante" name="question_hist_3" class="qsuivante"/>
    </form>
    </body>
    </html>"""

    print(html)

"""
if form.getvalue("rep_2_4"):
    score_hist_2 = score_hist_2 + 5
    
else:
    score_hist_2 -= 5
"""
open('resultats.csv', 'r')


if form.getvalue("date_regne"):
    dates = form.getvalue("date_regne")
    if dates == '1226 à 1270':
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass
























