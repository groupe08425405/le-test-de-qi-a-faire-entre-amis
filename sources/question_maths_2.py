# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("question_maths_2"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!doctype html>
    <html lang=fr>
        <head>
            <title> Partie Maths : Question 2</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
        
           <h6> Deuxième question </h6>
           <h2> Quel est le nombre qui vient compléter la célèbre suite suivante ? </h2>
           <p><img src="Images/suite_fibonacci.png"/></p>
           
           <form method="post" action="question_maths_3.py">
               <p><label for="rep_fibo"> Votre proposition : </label> <input type="text" name="rep_fibo" required/></p>
               <br/><br/>
               <input type="submit" value="Question suivante" class="qsuivante" name="question_maths_3"/>
           </form> 
        </body>
    </html>
"""

    print(html)

open('resultats.csv', 'r')

if form.getvalue("rep_fibo"):
    dates = form.getvalue("rep_fibo")
    if dates == "34":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass