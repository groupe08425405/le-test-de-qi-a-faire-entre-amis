# -*- coding: utf-8 -*-



import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_maths_5"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Maths : Question 5</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h5> Cette question était la plus dure. En effet, elle nécessite quelques connaissances. </h5>
           <h2> Ici aussi, il y avait deux manière de trouver la bonne réponse : </h2>
           <h2> Soit avec le binôme de Newton : </h2>
           <p><img src="Images/correction_maths_5.jpg"/></p>
           <h2> Soit avec le triangle de Pascal, qui nous donne les coefficients à placer devant les nombres x et y dont les puissances "se croisent" selon une certaine logique : </h2>
           <p><img src="Images/triangle_Pascal.png"/></p>
           <h2> la solution est donc : </h2>
           <p><img src="Images/solution_question_5.jpg"/></p>
           <form method="post" action="correction_enigmes.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_enigmes"/>
           </form> 
        </body>
    </html>
    """

    print(html)
