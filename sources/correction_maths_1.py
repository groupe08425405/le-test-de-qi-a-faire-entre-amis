# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_maths_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Maths : Question 1</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Le tableau de cette question devait être lu en ligne. En effet, ici, il fallait remarquer que, lorsqu'on additionne le double du nombre du milieu de la ligne, avec le premeir nombre de celle-ci, le résultats est le nombre écrit à la fin de la ligne. </h2>
           <h2> D'après la première ligne, on obtient : 2 + 2x7 = 16. </h2>
           <h2> Avec la deuxième ligne, on obtient : 9 + 2x20 = 49. </h2>
           <h2> Ainsi, 12 + 2x34 = 80, était la bonne réponse ! </h2>
           <form method="post" action="correction_maths_2.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_maths_2"/>
           </form> 
        </body>
    </html>
    """

    print(html)
