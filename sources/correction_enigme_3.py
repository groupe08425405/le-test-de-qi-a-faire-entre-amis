# -*- coding: utf-8 -*-




import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_enigme_3"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Enigme 3</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Cette famille compte 9 personnes : les deux parent, les six garçons ainsi que leur unique soeur. </h2>
           <form method="post" action="correction_enigme_4.py">               
               <input type="submit" class="qsuivante" value="Voir mes résultats" name="correction_enigme_4"/>
           </form> 
        </body>
    </html>
    """

    print(html)