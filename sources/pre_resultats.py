# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("pre_resultats"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie JO : Question 3 </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h5> Votre test est (enfin) terminé ! </h5>
           <h2>Etes-vous prêts à avoir votre résultats ?</h2>
           <form method="post" action="resultats.py">               
               <input type="submit" class="qsuivante" value="Voir mes résultats" name="resultats"/>
           </form> 
        </body>
    </html>
    """

print(html)
