# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_JO"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction JO</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <p> Enfin, voici la correction des questions sur les JO ! </p>
           <form method="post" action="correction_JO_1.py">               
               <input type="submit" class="qsuivante" value="Voir mes résultats" name="correction_JO_1"/>
           </form> 
        </body>
    </html>
    """

    print(html)
