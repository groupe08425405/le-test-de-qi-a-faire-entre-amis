# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("question_maths_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!doctype html>
    <html lang=fr>
        <head>
            <title> Partie Maths : Question 1</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
        
           <h6> Première question </h6>
           <form method="post" action="question_maths_2.py">
                <h2> Quel nombre vient completer la logique de l'énigme qui suit ? </h2>
                
                <table>
                    <tr>
                        <td> 2 </td>
                        <td> 7 </td>
                        <td> 16 </td>
                    </tr>
                    <tr>
                        <td> 9 </td>
                        <td> 20 </td>
                        <td> 49 </td>
                    </tr>
                    <tr>
                        <td> 12 </td>
                        <td> 34 </td>
                        <td> ??? </td>
                    </tr>
                </table>
                <p><label for="reponse"> Votre réponse : </label> <input type="text" name="reponse" required/></p>
                <br/><br/>
                <input type="submit" class="qsuivante" value="Question suivante" name="question_maths_2"/>
            </form>
        </body>
    </html>"""
    
    print(html)

open('resultats.csv', 'r') 
   
if form.getvalue("reponse"):
    dates = form.getvalue("reponse")
    if dates == "80":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
