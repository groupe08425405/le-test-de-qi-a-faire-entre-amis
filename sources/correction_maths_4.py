# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_maths_4"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Maths : Question 4</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Ce nombre (10^100) est appelé un gogol. Plutôt spécial comme nom ! </h2>
           <form method="post" action="correction_maths_5.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_maths_5"/>
           </form> 
        </body>
    </html>
    """

    print(html)