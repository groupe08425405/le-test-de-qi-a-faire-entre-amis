# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()

score_enigme_5 = 0

if form.getvalue("enigme_5"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie Enigmes : Question 5</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h4> Je n'ai ni frère ni soeur mais le fils de cet homme est le fils de mon père. Qui suis-je ?</h4>
           <form method="post" action="accueil_question_JO.py">
               <p><label for="rep_E_5"> Votre proposition : </label> <input type="text" name="rep_E_5" required/>  </p></br>
               <input type="submit" value="Question suivante" class="qsuivante" name="accueil_question_JO"/>
           </form> 
        </body>
    </html>
    """

    print(html)

if form.getvalue("rep_E_5") == 'Mon père':
    score_enigme_5 += 5
else:
    score_enigme_5 -= 5
    
if form.getvalue("rep_E_5"):
    reponse = form.getvalue("rep_E_5")
    if reponse == "Mon père":
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass