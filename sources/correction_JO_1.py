# -*- coding: utf-8 -*-



import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_JO_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction JO Question 1</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <p> Correction question 1 sur JO </p>
           <form method="post" action="correction_JO_2.py">               
               <input type="submit" class="qsuivante" value="Voir mes résultats" name="correction_JO_2"/>
           </form> 
        </body>
    </html>
    """

    print(html)
