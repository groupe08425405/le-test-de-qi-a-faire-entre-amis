# -*- coding: utf-8 -*-

# page apres clique du bouton "Commencer" sur la page principale

import cgitb
import cgi


# débogage
cgitb.enable()

# récupération données formulaire
form = cgi.FieldStorage()

if form.getvalue("commencer"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
          <meta charset="UTF_8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Morpion</title>
          <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
            <div class="header">
              <h4>Le Morpion !</h4>
            </div>
            <pre>
                <h2>Jouez autant que vous le souhaitez !
                Il s'agit d'un échauffement en attendant le véritable test.</h2>
            </pre>
            <div class="box">
              <div class="row">
                <input class="btn" type="text" readonly>
                <input class="btn" type="text" readonly>
                <input class="btn" type="text" readonly>
              </div>
              <div class="row">
                <input class="btn" type="text" readonly>
                <input class="btn" type="text" readonly>
                <input class="btn" type="text" readonly>
              </div>
              <div class="row">
                <input class="btn" type="text" readonly>
                <input class="btn" type="text" readonly>
                <input class="btn" type="text" readonly>
              </div>
            </div>
            <h2><p class="result">Joueur actuel : X</p></h2>
            <div class="reset">
              <button id="reset">Recommencer</button>
            </div>
            <form method="post" action="accueil_hist.py">
                <input type="submit" value="Continuer" class="continuer" name="accueil_hist">
            </form>
            <script src="css_js/fichier.js"></script>
          </body>
    </html>
    """
    
    print(html)
