# -*- coding: utf-8 -*-


import csv




print("Content-type: text/html; charset=UTF_8\n")
html = """<!doctype html>
<html lang=fr>
<head>
    <title>Le test de QI à faire entre amis !</title>
    <meta charset="UTF_8">
    <link rel="stylesheet" href="css_js/style1.css">
</head>

<body>

    <h1 class="titre_1">Le test de QI à faire entre amis !</h1>
    <h2 class="titre_2">Bienvenue sur la page principale !</h2>
    <h2 class="titre_3">Cliquez sur "Commencer" pour débuter le test.</h2>
    
    <form method="post" action="morpion.py">
        <input type="submit" class="bouton1" value="Commencer" name="commencer"/>
    </form>
    
</body>
</html>
"""

print(html)

data = [100]


with open('resultats.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(int(data))
