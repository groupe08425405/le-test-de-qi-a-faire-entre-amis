# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_hist_5"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Histoire : Question 5</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Pierre de Courbertin est un hisorien et pédague français, né en 1863 et mort en 1937.</h2>
           <h2> C'est donc en 1894 qu'on eu lieu les premiers Jeux Olympiques modernes, suite à son idée d'internationaliser le sport.</h2>
           <p><img src="Images/Coubertin.jpg"/></p>
           <form method="post" action="correction_maths.py">               
               <input type="submit" class="qsuivante" value="Suivants" name="correction_maths"/>
           </form> 
        </body>
    </html>
    """

    print(html)

