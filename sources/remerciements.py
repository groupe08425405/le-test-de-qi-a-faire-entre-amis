# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("remerciements"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Merci !</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style.css">
        </head>
        <body>
           <p> Merci d'avoir été au bout de ce test ! A bientôt, en espérant que vous avez appris des choses ! </p>
        </body>
    </html>
    """

    print(html)


### effet neige en style ???