# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_hist_3"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Histoire : Question 3 </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Sydney est la ville la plus peuplée d'Australie et du continent océanien. </h2>
           <h2> C'est le capitaine britannique Arthur Phillip qui donna son nom à la ville que nous appelons aujourd'hui "Sydney".</h2>
           <p><img src="Images/Sydney.jpg"/></p>
           <form method="post" action="correction_hist_4.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_hist_4"/>
           </form> 
        </body>
    </html>
    """

    print(html)

