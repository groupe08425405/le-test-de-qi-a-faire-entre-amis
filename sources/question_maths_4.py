# -*- coding: utf-8 -*-

import cgi
import cgitb
import csv


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("question_maths_4"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Partie Maths : Question 4</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h6>Comment nomme-t-on le nombre suivant ?</h6>
           <p><img src="Images/maths_4.jpg"/></p>
               
           <form method="post" action="question_maths_5.py">
               <p><input type="radio" name="nom_nb" value="rep_1"/> <label for="nom_nb">Un "Big Number"</label><br/>
               <input type="radio" name="nom_nb" value="rep_2"/> <label for="nom_nb">Un "Gargantua"</label><br/>
               <input type="radio" name="nom_nb" value="rep_3"/> <label for="nom_nb">Un "Gogol"</label><br/></p>
               <br/> <br/>
               <input type="submit" value="Question suivante" class="qsuivante" name="question_maths_5"/>
           </form> 
        </body>
    </html>
    """

    print(html)


open('resultats.csv', 'r')

if form.getvalue("nom_nb"):
    nom = form.getvalue("nom_nb")
    if nom == 'Un "Gogol"':
        score = 5
    else:
        score = -5
        
    data = score
    fichier_csv = open('resultats.csv', 'w')
    obj = csv.write(fichier_csv)
    obj.write(data)
else:
    pass