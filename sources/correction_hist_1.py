# -*- coding: utf-8 -*-

import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()


if form.getvalue("correction_hist_1"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction Histoire : Question 1</title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Napoléon Bonaparte, né le 15 août 1769 à Ajaccio et mort le 5 mai 1821 sur l'île de Sainte-Hélène, est un militaire et homme d'Etats français.</h2>
           <h2> Il est le premier empeureur des Français du 18 mai 1804 au 6 avril 1814 et du 20 mars au 22 juin 1815, sous le nom de Napoléon Ier </h2>
           <p><img src="Images/Napoleon_Bonaparte.jpg"/></p>
           <form method="post" action="correction_hist_2.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_hist_2"/>
           </form> 
        </body>
    </html>
    """

    print(html)