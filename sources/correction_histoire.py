# -*- coding: utf-8 -*-


import cgi
import cgitb


cgitb.enable()
form = cgi.FieldStorage()

score_JO_3 = 0

if form.getvalue("correction_histoire"):
    print("Content-type: text/html; charset=UTF_8\n")
    html = """<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title> Correction de la partie Histoire </title>
            <meta charset="UTF_8"/>
            <link rel="stylesheet" href="css_js/style1.css">
        </head>
        <body>
           <h2> Désormais, voilà la correction des questions d'histoire </h2>
           <form method="post" action="correction_hist_1.py">               
               <input type="submit" class="qsuivante" value="Suivant" name="correction_hist_1"/>
           </form> 
        </body>
    </html>
    """

    print(html)